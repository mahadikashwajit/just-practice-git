import React, { Component } from 'react';
// import Select from 'react-select';

class RegisterForm extends Component {
    state = {
        name: '',
        email: '',
        gender: ''


    }

    data = {

    }

    onSubmit = (e) => {
        e.preventDefault();

        var data = {
            id: 4,
            name: this.state.name,
            email: this.state.email,
            gender: this.state.gender
        }
        console.log("details", data)


        this.props.addNewDetail(data)


    }

    onChangeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value }
            // , () => console.log(this.state.gender)
        )

    }

    onSelect = (e) => {
        this.setState({ gender: this.refs.select.value })
        console.log("Function Run", this.state.gender)

    }


    render() {
        return (
            <div>

                <form onSubmit={this.onSubmit}>
                    <div className='form-group'>
                        <label style={makeBold}>Your Name</label>
                        <input className='form-control'
                            type='text'
                            placeholder='Your Name'
                            name='name'
                            value={this.state.name}
                            onChange={this.onChangeHandler}
                        />
                    </div>

                    <div className='form-group'>
                        <label style={makeBold}>Email ID</label>
                        <input className='form-control'
                            type='email'
                            placeholder='Your Email'
                            name='email'
                            value={this.state.email}
                            onChange={this.onChangeHandler}
                        />
                    </div>

                    <div>
                        <label style={makeBold}>Gender</label>
                        <select className='form-control'
                            ref='select'

                            onChange={this.onSelect}>

                            <option >Select Gender..</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="Others">Others</option>

                        </select>

                    </div>

                    <div>
                        <button type="submit" className="btn btn-primary m-1">Submit</button>
                    </div>
                </form>

            </div >
        )
    }

}

const makeBold = {
    fontWeight: 'bold',
}




export default RegisterForm;