import React from 'react';
import HomeData from "./HomeData"

function Home(props) {

    const mapOnData = props.data_p.map((i) => <HomeData key={i.id}
        onDelete={props.onDelete}
        data_p={i} />);

    return (
        <div>
            <h1 style={headingStyle}>Home</h1>
            {mapOnData}

        </div>

    )
}

const headingStyle = {
    background: '#1CEDC7',
    color: 'white',
}

export default Home;