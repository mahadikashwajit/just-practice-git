import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navbar extends Component {
    state = {}

    render() {
        return (
            <header style={headerStyle}>
                <h1>Project 1</h1>
                <Link to="/" style={linkStyle}>Home</Link>
                <Link to="/register" style={linkStyle}>Register</Link>
            </header>
        );
    }
}


const headerStyle = {

    background: '#333',
    color: '#fff',
    textAlign: 'center',
    padding: '10px'
}

const linkStyle = {

    color: 'white',
    padding: '10px'
}
export default Navbar;