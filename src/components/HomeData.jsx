import React, { Component } from 'react';

class HomeData extends Component {
    state = {};

    dataStyle = {
        border: '10px #808080 solid',
        padding: '10px',
        margine: '0px'


    }

    btnStyle = {
        float: 'right',

    }
    render() {
        const { id, name, email, gender } = this.props.data_p
        { console.log("ID", id) }
        return (
            <div>

                <div style={this.dataStyle}>
                    <h4 >{name}</h4>
                    <button className='btn btn-danger' style={this.btnStyle} onClick={() => this.props.onDelete(id)}>Delete</button>
                    <h4 >{email}</h4>
                    <h4 >{gender}</h4>
                </div>

            </div>
        );
    }
}

export default HomeData;