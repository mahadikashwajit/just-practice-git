import React, { Component } from 'react';
import Navbar from "./components/Navbar"
import Home from "./components/Home"
import HomeData from "./components/HomeData"
import Register from "./components/Register"
// import RegisterForm from './components/RegisterForm';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {


  state = {

    data: [
      {
        id: 1,
        name: 'Ranjeet',
        email: 'ranjeet@email.com',
        gender: 'male'
      },

      {
        id: 2,
        name: 'Vishali',
        email: 'Vishali@email.com',
        gender: 'female'
      }
    ]
  }

  addNewDetail(obj) {

    var data = this.state.data
    data.push(obj)
    this.setState({
      data: data
    });
    console.log(obj)
  }

  onDelete = (id) => {
    console.log("Delete Button Clicked", id);
    const toDel = this.state.data.filter((i) => i.id !== id);
    this.setState({ data: toDel })
    console.log(this.state)
  }


  render() {

    return (
      <Router>
        <Navbar />

        <Switch>
          {/* <Route path='/' component={Home} exact /> */}
          <Route path='/' exact render={props => (
            <React.Fragment>
              <Home key={this.state.id}
                data_p={this.state.data}
                onDelete={this.onDelete}
              />
            </React.Fragment>
          )} />

          <Route path='/register' render={props => (
            <React.Fragment>
              <Register key={this.state.id}
                addNewDetail={this.addNewDetail.bind(this)}

              />
            </React.Fragment>


          )} />



        </Switch>




      </Router>



    );
  }

}

export default App;
